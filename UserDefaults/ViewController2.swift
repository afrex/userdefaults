//
//  ViewController2.swift
//  UserDefaults
//
//  Created by alexfb on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController2: UIViewController, Viewcontroller3DDelegate {

  
    @IBOutlet weak var infoLabel: UILabel!
    var msg:String!
    //el viewdidload solo pasa una vez, mientras que los otros pasan las veces necesarias al cargar la vista
    //userdefaults sirve para guardar la informacion
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let msg=UserDefaults.standard.object(forKey: "mensaje") as? String else{
            return
        }
        infoLabel.text=msg
        
        // Do any additional setup after loading the view.
    }
    

    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(infoLabel.text!, forKey: "mensaje")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func salirPressed(_ sender: Any) {
        UIView.animate(withDuration: 1, animations: {
            self.view.backgroundColor=UIColor.white
        }) { (isTrue) in
            UserDefaults.standard.set(false, forKey: "hola")
            self.dismiss(animated: false, completion: nil)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func ingresoMensaje(msg: String) {
        infoLabel.text=msg
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let dvc=segue.destination as! ViewController3
        dvc.delegate=self
    }
}
