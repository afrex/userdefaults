//
//  ViewController.swift
//  UserDefaults
//
//  Created by alexfb on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "hola") as? Bool==true{
performSegue(withIdentifier: "welcome", sender: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(true, forKey: "hola")
    }
    
    @IBAction func login(_ sender: Any) {
        performSegue(withIdentifier: "welcome", sender: self)
    }


    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func salirLogin(segue:UIStoryboardSegue){
        
    }
}

