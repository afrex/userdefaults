//
//  ViewController3.swift
//  UserDefaults
//
//  Created by alexfb on 30/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
@objc protocol Viewcontroller3DDelegate:class {
    func ingresoMensaje(msg:String)

    
    @objc optional func funcOptional()
    
}

class ViewController3: UIViewController {

    @IBOutlet weak var messageTextField: UITextField!
    var delegate:Viewcontroller3DDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func aceptarPressed(_ sender: Any) {
        
        delegate.ingresoMensaje(msg: messageTextField.text!)
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
